from setuptools import setup

setup(
    name='parquetview',
    version='1.0.0',
    author='Gregory Werbin',
    py_modules=['parquetview'],
    install_requires=['pyarrow', 'chubb-ads-utils'],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'parquetview = parquetview:main'
        ]
    }
)

