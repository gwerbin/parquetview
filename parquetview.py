import argparse
import os
import subprocess
import sys

import pyarrow.parquet as pq

from chubb_ads.utils.cli import PathType


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path_or_filename', type=PathType(must_exist=True))
    args = parser.parse_args()
    path = args.path_or_filename

    if path.is_dir():
        path = path / os.listdir(path)[0]

    print(pq.read_schema(str(path)))


if __name__ == '__main__':
    main()
